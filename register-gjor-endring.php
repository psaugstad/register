<?php
$attrib_AC=5;

include 'mysql_db.php';
include 'constants.php';

function echo_text()
{
if (!isset($_POST['TABLE'])) {
    echo "Ingen databasetabell angitt (gjør endring)";
    return false;
}

$post = $_POST;
$message = 'No message';
$id = $post['id'];
$status = 'OK';
$new_id = $post['new'] == 'true';
unset ($post['new']);
unset ($post['TABLE']);
if (isset ($post['Endre'])) {
    unset ($post['Endre']);

    // Fjern fra $post -> alle orig_<key> og dersom ingen endring <key>
    foreach ($post as $key => $item) {
        if (preg_match('/^orig_(.+)/', $key, $match)) {
            if (($item == $post[$match[1]]) && !$new_id) unset ($post[$match[1]]);
            unset ($post[$key]);
        }
    }
    if (!preg_match('/(.+)_history$/', $_POST['TABLE'], $match)) {
        unset ($post['id']);
    }
    
#    print '<pre>';
#    print_r ($post);
#    print '</pre>';

    $exe_arr = array();
    $qm_arr = array();
    if (count($post)) {
        $conn = open_mysql();
        foreach ($post as $key => $item) {
            $exe_arr[] = trim($item);
            $qm_arr[] = '?';
        }
        if ($new_id) {
            $query = "INSERT INTO ".$_POST['TABLE']." (".implode (', ', array_keys($post)).") VALUES (".implode (', ', $qm_arr).")";
            $message = "Satt inn ny id";
        } else {
            $query = "UPDATE ".$_POST['TABLE']." SET ".implode (' = ?,', array_keys($post))." = ? where id='".$id."'";
            $message = "Endret gjenstand med id '".$id."'";
        }
        
        $prep = $conn->prepare($query);
        $result = $prep->execute($exe_arr);
        if ($result && $new_id && !isset($post['id'])) $id = $conn->lastInsertId();
        error_log ("Try: prepare -> $query | exe -> ".print_r($exe_arr, true)." | id -> $id\n");
        if (!$result) {
            $status = 'Error';
            error_log ("Didn't work: ErrorInfo -> ".print_r($conn->errorInfo(), true)."\n");
            if ($new_id) $message = "Noe gikk galt, prøv å endre 'Oppdatert' bittelitt";
            else $message .= " men noe gikk galt, så ingenting ble endret likevel";
        }
        close_mysql($conn);
    } else {
        $message = 'Ingen data er endret';
    }
}
echo "{\"status\": \"$status\", \"text\": \"$message\", \"id\": \"$id\"}";

return true;
}
header('Content-Type: text/html');
session_start(); if ($_SESSION['AC'] >= $attrib_AC) echo_text(); else echo 'Ingen tilgang';
?>
