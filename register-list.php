<?php
$attrib_AC=4;

include '_utils/tabelize_arr.php';
include '_utils/transpose_arr.php';
include 'mysql_db.php';
include 'constants.php';
include '_medlemsregister/medlems_support.php';

function echo_text($can_store)
{
if (!isset($_POST['TABLE'])) {
    echo "Ingen databasetabell angitt (endre-medlem)";
    return false;
}
?>
<script type="text/javascript">
<!--
var db_table = '<?= $_POST['TABLE'] ;?>';
var endre = '<?= $_POST['endre'] ;?>';
var can_store = <?= $can_store ;?>;
$(document).ready(function() 
{
    if (endre == true) {
        $('#show_status').html('');
    }
}
);
function checkListSubmit() {

    if (!can_store) {
        alert("Ikke tilstrekkelige rettigheter til å endre");
    } else if (confirm ('Lagre?')){
        var form_query_string = $("#change_list").serialize();
        form_query_string += '&TABLE='+db_table;
        $.post("register-gjor-endring.php", form_query_string,
        function(data){
            $('#show_status').html(data.text);
            if (data.status == 'OK') {
                if (data.id) {
                    register_list(data.id, 0);
                }
            }
        }, "json");
    }
}
//-->
</script>
<?php

//print '<pre>'; print_r ($post); print '</pre>';

$my_err = 0;
$id = $_POST['id'];

$conn = open_mysql();

$q = $conn->query("DESCRIBE ".$_POST['TABLE']);

$desc = array();
while($row = $q->fetch(PDO::FETCH_ASSOC)) {
    $desc[$row['Field']] = $row['Type'];
}

$query = "select * FROM ".$_POST['TABLE']." where id='".$id."'";
$result = $conn->query($query);
if (!$result) {
//    echo mysql_error().'<br>';
//    close_mysql($conn);
    $my_err = 1;
}

$Vis = array();

if (!$my_err) {
    // Print out result
    $rows = $result->fetchall(PDO::FETCH_ASSOC);
    $number_of_rows = count($rows);
    if ($number_of_rows) {
        foreach ($rows as $row) {
            $Vis[] = array_keys($row);
            $this_row = $row;
            if ($_POST['endre']) foreach ($row as $key => $item) {
                $item = htmlspecialchars($item);
                $mod_item = '<input name="'.$key.'" type="text" value="'.$item.'">';
                if ($key == 'id') {
                    $mod_item = $item;
                }
                if (preg_match ('/^enum/', $desc[$key] )) {
                    $mod_item = enum_option_list($key, $item, $desc[$key]);
                }
                $mod_item .= '<input name="orig_'.$key.'" id="orig_'.$key.'" type="hidden" value="'.$item.'">';
                $this_row[$key] = $mod_item;
            }
        }
        $Vis[] = array_values($this_row);
    } else {
        $Vis = array(array('Ingen data for nr. '.$id));
    }
    close_mysql($conn);
}
?>
<form id="change_list"><div>
<input type="hidden" name="Endre" value="1">
<input type="hidden" name="id" value="<?= $id ;?>">
<?php
if ($_POST['TRANSPOSE'] == 'yes') $Vis = transpose_arr($Vis);
tabelize_arr($Vis);
?>
</div></form>
<?php if ($number_of_rows) if ($_POST['endre']) { ?>
<a href="javascript:register_list(<?= $id ;?>, 0);">&nbsp;Angre&nbsp;</a>
&nbsp;&nbsp;<a href="javascript: checkListSubmit();">&nbsp;Lagre&nbsp;</a>
<?php } else if ($can_store) { ?>
<a href="javascript:register_list(<?= $id ;?>, 1);">Endre</a>
<?php } 



return true;
}
header('Content-Type: text/html');
session_start(); if ($_SESSION['AC'] >= $attrib_AC) echo_text($_SESSION['AC'] > $attrib_AC ? 1 : 0); else echo 'Ingen tilgang';
?>
