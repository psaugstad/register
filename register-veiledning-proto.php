<?php

function echo_text($al)
{

?>
<h1>Hvordan bruke registerene over eiendeler</h1>

<a name="instrumenter" href="#instrumenter">Instrumenter</a> &nbsp;
<a href="#uniformer">Uniformer</a> &nbsp;
<a href="#noter">Noter</a>
<hr>
<h1>Instrumenter</h1>
<h2>Søkefelt</h2>

<p>Fritekst-søk i alle kolonner etter alle ordene
Søkefeltet har flere bruksmåter: Søke med fritekst ord for ord over alle kolonner, eller søke bare innen gitte kolonner</p>
<h3>Fritekst</h3>
<p>Søk etter ett eller flere ord. Linjer der alle ordene forekommer i en eller flere av kolonnene vil bli vist (uansett store og små bokstaver)</p>
<h3>Kolonner</h3>
<p>Formatet er Kolonnenavn<i>&lt;kriterium&gt;</i>søketekst. Merk at store og små bokstaver i kolonnenavnet har betydning. Kriterium kan være lik,
større eller lik eller mindre. Dette kan gjentas for flere kolonner, der mellomrom skiller kolonnene. Eksakt søk er også mulig.</p>
<p>Eksempler
<ul>
    <li>Instrument=nett - finner både Kornett og Klarinett i kolonnen Instrument</li>
    <li>id=30 - finner alle id der 30 inngår (130, 307, 30 osv.)</li>
    <li>id='30' - finner eksakt id</li>
    <li>id&gt;30 - finner alle id større eller lik 30</li>
    <li>id&lt;30 - finner alle id mindre enn 30</li>
    <li>Fra&gt;2008 Oppdatert&lt;2014-01-01 - finner alle som er fra 2008 eller senere der loggen er eldre enn 2014-01-01</li>
</ul>

</p>

</p>
<h2>Hvilke</h2>
<p>Her er det fire valg: <b>Våre</b> - Instrumenter som korpset eier. <b>I aktiv bruk</b> - Instrumenter som er registrert utdelt til noen.
<b>På lager</b> - Instrumenter som ikke er registrert utdelt til noen. <b>Alle</b> - Alle instrumenter i registeret, også de som er registrert
som utgått.
</p>
<h2>Kolonnene i tabellen</h2>
<p>Nesten alle opplysninger er gitt i tabellen.
Kolonnene t.o.m. 'Serienr' er hoved-informasjonen om noten. Resten av kolonnene inngår i historikken til instrumentene.
Det er kun den siste registreringen av historikken som vises i hoved-bildet.
Man får all informasjon for en note ved å trykke på linken i kolonne 1.
<h3>Sortering av tabellen</h3>
<p>Tabellen er i utgangspunktet ikke sortert. Man kan sortere på hvilken kolonne man vil ved å trykke på overskriften til kolonnen, første gang alfabetisk, neste gang omvendt
alfabetisk.</p>
<h2>Endre registreringen</h2>
<p>Når man trykker på linken i første kolonne får man detaljert informasjon om instrumentet.</p>
<h3>Hoved-informasjon</h3>
<p>Kolonnene 'Instrument', 'Type', 'Produsent_Modell' og 'Serienr' kan endres permanent ved å trykke på endre-knappen,
endre noe og så trykke på 'Lagre' (eller la være ved å trykke på 'Angre').</p>
<h3>Historikk-informasjon</h3>
<p>Kolonnene 'Tilstand', 'Eier', 'Medlem' og 'Logg' er historiske data.<br>
'Tilstand' har 5 graderinger fra 'Ypperlig' til 'Ikke brukbart'<br>
'Medlem' brukes til å registrere hvilket medlem som har instrumentet, eller <i>lager</i> evt. <i>uvisst</i><br>
'Logg' kan inneholde ikke permanent informasjon som f. eks. 'overhalt - kr. 560', 'falt ned trappene' o.l.<br>
Alle historisk info blir datert, slik at ytterligere datering i Logg-kolonnen bør være overflødig.
De siste to kolonnene blir auto-innfyllt med dato og hvem som gjør tilføyelsen.</p>
<h3>Registrere ny</h3>
<p>Fra hovedbildet må man først velge et tilfeldig instrument. Herfra trykker man på 'nytt'
for så å legge inn registreringen og så trykke på lagre.
</p>
<a name="uniformer" href="#instrumenter">Instrumenter</a> &nbsp;
<a href="#uniformer">Uniformer</a> &nbsp;
<a href="#noter">Noter</a>
<hr>
<h1>Uniformer</h1>
<h2>Søkefelt</h2>

<p>Søkefeltet brukes på to måter: Søke med fritekst over alle kolonner, eller søke bare innen én kolonne</p>
<h3>Fritekst</h3>
<p>Søk etter ett eller flere ord. Linjer der alle ordene forekommer i en eller flere av kolonnene vil bli vist (uansett store og små bokstaver)</p>
<h3>Kolonne</h3>
<p>Formatet er Kolonnenavn=søketekst. Merk at store og små bokstaver i kolonnenavnet har betydning.
I søketeksten har ikke store/små noen betydning.
Se Instrumenter (over) for avanserte søk.
</p>
<h2>Hvilke</h2>
<p>Her er det endel valg for å grov-sortere</p>
<h2>Kolonnene i tabellen</h2>
<p>Nesten alle opplysninger er gitt i tabellen.
<p>Nesten alle opplysninger er gitt i tabellen.
Kolonnene t.o.m. 'Serienr' er hoved-informasjonen om noten. Resten av kolonnene inngår i historikken til instrumentene.
Det er kun den siste registreringen av historikken som vises i hoved-bildet.
Man får all informasjon for en note ved å trykke på linken i kolonne 1.
<h3>Sortering av tabellen</h3>
<p>Tabellen er i utgangspunktet ikke sortert. Man kan sortere på hvilken kolonne man vil ved å trykke på overskriften til kolonnen, første gang alfabetisk, neste gang omvendt
alfabetisk.</p>
<h2>Endre registreringen</h2>
<p>Når man trykker på linken i første kolonne får man detaljert informasjon om uniforms-effekten.</p>
<h3>Hoved-informasjon</h3>
<p>Kolonnene 'Uniformseffekt', 'Storrelse', 'Produsent_Modell' og 'Serienr' kan endres permanent ved å trykke på endre-knappen,
endre noe og så trykke på 'Lagre' (eller la være ved å trykke på 'Angre').</p>
<h3>Historikk-informasjon</h3>
<p>Kolonnene 'Tilstand', 'Medlem' og 'Logg' er historiske data.<br>
'Tilstand' har 5 graderinger fra 'Ypperlig' til 'Ikke brukbart'<br>
'Medlem' brukes til å registrere hvilket medlem som har instrumentet, eller <i>lager</i> evt. <i>uvisst</i><br>
'Logg' kan inneholde ikke permanent informasjon som f. eks. 'flenge nær venstre kne pga. fall' o.l.<br>
Alle historisk info blir datert, slik at ytterligere datering i Logg-kolonnen bør være overflødig.
De siste to kolonnene blir auto-innfyllt med dato og hvem som gjør tilføyelsen.</p>
<h3>Registrere ny</h3>
<p>Fra hovedbildet må man først velge en tilfeldig uniformseffekt. Herfra trykker man på 'nytt'
for så å legge inn registreringen og så trykke på lagre.
</p>
<a name="noter" href="#instrumenter">Instrumenter</a> &nbsp;
<a href="#uniformer">Uniformer</a> &nbsp;
<a href="#noter">Noter</a>
<hr>
<h1>Noter</h1>

<h2>Søkefelt</h2>

<p>Søkefeltet brukes på to måter: Søke med fritekst over alle kolonner, eller søke bare innen én kolonne</p>
<h3>Fritekst</h3>
<p>Søk etter ett eller flere ord. Linjer der alle ordene forekommer i en eller flere av kolonnene vil bli vist (uansett store og små bokstaver)</p>
<h3>Kolonne</h3>
<p>Formatet er Kolonnenavn=søketekst. Merk at store og små bokstaver i kolonnenavnet har betydning.
I søketeksten har ikke store/små noen betydning.
Se Instrumenter (over) for avanserte søk.
</p>
<h2>Hvilke</h2>
<p>Her er det to valg: Std - alle noter med tilstand 'Bra'. Alle - Alle registreringer, også noter som er
'Ikke brukbar' og 'Makulert'</p>
<h2>Kolonnene i tabellen</h2>
<p>Nesten alle opplysninger er gitt i tabellen.
Kolonnene t.o.m. 'Merknad' er hoved-informasjonen om noten. Resten av kolonnene inngår i historikken til notene.
Det er kun den siste registreringen av historikken som vises i hoved-bildet.
Man får all informasjon for en note ved å trykke på linken i kolonne 1.
<h3>Sortering av tabellen</h3>
<p>Tabellen er i utgangspunktet sortert etter 'Ref'. Desverre er sorteringen ikke særlig smart, så rekkefølgen for 'Ref' blir ikke
akkurat som forventet; A19 - A2 - A20 blir rekkefølgen fordi sifferene i ref ikke oppfattes som tall av sorterings-algoritmen.
Man kan sortere på hvilken kolonne man vil ved å trykke på overskriften til kolonnen, første gang alfabetisk, neste gang omvendt
alfabetisk.</p>
<h2>Endre registreringen</h2>
<p>Når man trykker på linken i første kolonne får man detaljert informasjon om noten.</p>
<h3>Hoved-informasjon</h3>
<p>Kolonnene 'Ref', 'Tittel', 'Komponist', 'Arr', 'Grad' og 'Merknad' kan endres permanent ved å trykke på endre-knappen,
endre noe og så trykke på 'Lagre' (eller la være ved å trykke på 'Angre').
Feltet 'Merknad' er ment for merknader av permanent type, så som sax-solo eller Overture eller Marsj o.l.</p>
<h3>Historikk-informasjon</h3>
<p>Kolonnene 'Tilstand' og 'Logg' er historiske data.<br>
'Tilstand' kan være 'Bra', 'Ikke brukbart' eller 'Makulert'<br>
'Logg' kan inneholde ikke permanent informasjon som f. eks. 'Høstkonsert', 'Trompetstemme mangler',
'For enkel slik som sammensetningen av musikanter er nå for tiden' o.l.<br>
Alle historisk info blir datert, slik at ytterligere datering i Logg-kolonnen bør være overflødig.
De siste to kolonnene blir auto-innfyllt med dato og hvem som gjør tilføyelsen.</p>
<h3>Registrere ny</h3>
<p>Fra hovedbildet må man først velge en tilfeldig note. Herfra trykker man på 'nytt'
for så å legge inn registreringen og så trykke på lagre.
Kolonnen 'Ref' angir hvor i det fysiske arkivet noten befinner seg. Denne kolonnen bør settes opp slik at den består
av en stor bokstav og så 1 til 3 siffer uten noen mellomrom. Bokstaven tilsvarer første bokstav i tittelen på noten.
Sifferene er et løpenummer innen denne bokstaven. Nye noter tilføyes etter høyeste nåværende nummer for denne bokstaven.
</p>
<?php
return true;
}
include 'i.php';
?>
