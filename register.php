<?php

include '_utils/tabelize_arr.php';
include '_utils/make_xls.php';
include '_medlemsregister/medlems_support.php';

function document_ready_javascript() {
?>
        $(".tablesorter").tablesorter();
        $(".selectable .check").click(function(e){
            e.stopImmediatePropagation();
        });
        $(".selectable tr").click(function(e){
            // stop normal click
            // e.preventDefault();
            var rowIndex = $(this).index()+1;
            $('.selectable').hide();
            $('#one_item').load("register-historikk?reg=<?= REGISTERTYPE ;?>&TRANSPOSE=<?= TRANSPOSE ;?>&row="+rowIndex);
        });
        $("#filter_ticks").click(function(e){
            e.preventDefault();
            document.both_choices.submit();
        });
<?php
}

function filter_rows ($search, &$rows) {
    $count_matches = 0;
    $column = array();
    $type = array();
    foreach ($search as $skey => $single) {
        $pre_type = '=';
        $column_arr = explode ($pre_type, $single);
        if (count($column_arr) < 2) {
            $pre_type = '>';
            $column_arr = explode ($pre_type, $single);
            if (count($column_arr) < 2) {
                $pre_type = '<';
                $column_arr = explode ($pre_type, $single);
            }
        }
        if (count($column_arr) == 2) {
            if (isset($rows[0][$column_arr[0]])) {
                $column[$skey] = $column_arr[0];
                if ($pre_type == '=') {
                    $quoted = explode ("'", $column_arr[1]);
                    if (isset ($quoted[2])) {
                        $type[$skey] = 'exact';
                        $search[$skey] = $quoted[1];
                    } else {
                        $type[$skey] = 'like';
                        $search[$skey] = $column_arr[1];
                    }
                } else {
                    $type[$skey] = $pre_type;
                    $search[$skey] = $column_arr[1];
                }
            }
        }
    }
    foreach ($rows as $key => $row) {
        $found = 1;
        $whole_row = implode (' ', $row);
        foreach ($search as $skey => $single) {
            $flatten = isset($column[$skey]) ? $row[$column[$skey]] : $whole_row;
            if ($type[$skey] == 'exact') {
                if ( strtolower($single) != strtolower($flatten)) $found = 0;
            } else if ($type[$skey] == '>') {
                if (!($flatten >= $single)) $found = 0;
            } else if ($type[$skey] == '<') {
                if (!($flatten < $single)) $found = 0;
            } else {
                $pattern = '/'.$single.'/i';
                if (!preg_match ($pattern, $flatten)) $found = 0;
            }
        }
        if ($found) $count_matches++;
        else unset ($rows[$key]);
    }
    return $count_matches;
}

function echo_text($al)
{
if (REGISTERTYPE == 'REGISTERTYPE') return false;
include INCLUDEFILE;
$reg_type = REGISTERTYPE;
$heading_type = HEADING;
$search_string = SEARCH_DEFAULT;
$my_err = 0;

$getPost = array();
if (count($_GET )) $getPost = $_GET;
if (count($_POST)) $getPost = array_merge ($getPost, $_POST);

$paramList = '';
$delim = '?';
if (count($getPost)) {
    foreach ($getPost as $key => $value) {
        $paramList .= $delim.$key.'='.urlencode($value);
        $delim = '&';
    }
}

if (isset($getPost['which'])) {
    $which_opt = $getPost['which'];
} else {
    $var = array_keys($groups);
    $which_opt = array_shift($var);
}
$update_time = array();
$conn = open_mysql();
$query = '
SELECT UPDATE_TIME
FROM   information_schema.tables
WHERE  TABLE_SCHEMA = "'.DBNAME.'"
   AND TABLE_NAME = "'.REGISTERTYPE.'"';
$result = $conn->query($query);
$update_time = $result->fetch(PDO::FETCH_NUM);
//     print "<pre>";print_r ($update_time);print "</pre>";

$my_err = 0;

$q = $conn->query("DESCRIBE ".$i_table);

$desc = array();
while($row = $q->fetch(PDO::FETCH_ASSOC)) {
    $desc[$row['Field']] = $row['Type'];
}

close_mysql($conn);

//echo '<pre>'; print_r($desc); echo '</pre>';
$Filter = array();
if (!$my_err) {
    // Print out result
    {
        $this_row = array();
        $head_row = array();
            foreach ($desc as $key => $item) {
                if (preg_match ('/^enum/', $desc[$key] )) {
                    $head_row[] = $key;
                    $mod_item = enum_tick_list($key, $item, $getPost);
                    //$mod_item .= '<input name="orig_'.$key.'" id="orig_'.$key.'" type="hidden" value="'.$item.'">';
                    $this_row[$key] = $mod_item;
                }
            }
            $Filter[] = $head_row;
            $Filter[] = array_values($this_row);
            //echo '<pre>'; print_r($this_row); echo '</pre>';
    }
}

echo '
<h1> '.$heading_type.' <small><small>&nbsp; &nbsp; &nbsp;  Sist endret: '.$update_time[0].'</small></small> </h1>

';
{
    $conn = open_mysql();

    $search = 0;
    if (isset($getPost['search'])) {
        $search_string = $getPost['search'];
    }
    if ($search_string) {
        $search = explode (' ',$search_string);
    }

    if ($which_opt == 'Alle') {
        $and_where = '';
        if (count($getPost)) {
            $prev_kw = '';
            $and = 'and (';
            foreach ($getPost as $key => $value) {
                $item_arr = explode ('_', $key);
                if (count($item_arr) == 2) {
                    if ($prev_kw == $item_arr[0]) {
                        $and_where .= ' or ';
                    } else {
                        $and_where .= $and;
                        $and = ') and (';
                    }
                    $prev_kw = $item_arr[0];
                    $and_where .= $item_arr[0]."='".$item_arr[1]."'";
                }
            }
            if ($and == ') and (') $and_where .= ')';
        }
        $filter = $common." ".$and_where." order by Kjent_med DESC ";
    } else {
        $filter = $groups[$which_opt]['SQL'];
    }
    $query = $filter;

    $result = $conn->query($query);
    if (!$result) {
//        echo mysql_error().'<br>';
//        close_mysql($conn);
        $my_err = 1;
        error_log ("Tried: query -> $query | Didn't work: ErrorInfo -> ".print_r($conn->errorInfo(), true)."\n");
    }
    if (is_array($search)) {
        foreach ($search as $key => $single) {
            if ($single == '') unset ($search[$key]);
        }
    }

    $number_of_rows = 0;
    $err_msg = '';
    if ($my_err) {
        $err_msg = 'Intern feil';
    } else {
        // Print out result
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        $number_of_rows = count ($rows);
        if (!$number_of_rows) {
            $err_msg = 'Ingen data funnet';
        }
    }
    if ($number_of_rows) {
        $tbl_heading = array_merge (array(' ' => ''), $rows[0]);
        if (isset($tbl_heading['Av'])) {
            unset($tbl_heading['Av']);
        }
        if (isset($tbl_heading['Medlem'])) $medlem_subst = medlem_substitute_arr($conn);
        $tbl_heading = array_keys($tbl_heading);
        $link_col = -1;
        foreach ($tbl_heading as $key => $val) {
            if ($val == 'Link') $link_col = $key;
        }
        foreach ($rows as $key => $row) {
            if (isset($row['Medlem']))    $rows[$key]['Medlem'] = medlem_substitute($medlem_subst, $row['Medlem']);
            if (isset($row['Oppdatert'])) {
                $var = explode (' ', $row['Oppdatert']);
                $rows[$key]['Oppdatert'] = array_shift ($var);
            }
            if (isset($row['Av']))  unset($rows[$key]['Av']);
        }
        $out_status = "Totalt $number_of_rows gjenstander i listen";
        if (is_array($search)) {
            $count_matches = filter_rows($search, $rows);
            $out_status = "Fant '".implode(' ', $search)."' i $count_matches av $number_of_rows linjer";
        }
        $Vis = array();
        if (count ($rows)) foreach ($rows as $row) {
        //     print "<pre>";print_r ($row);print "</pre>";
            array_unshift($row, '<input class="check" type="checkbox" />');
            if (isset($row['Link'])) {
                $val = trim($row['Link']);
                if ($val) {
                    $row['Link'] = '<a href="'.$val.'">link</a>';
                }
            }
            $Vis[] = array_values ($row);
        }
        array_unshift ($Vis, $tbl_heading);
    }
    close_mysql($conn);
}
?>
<div style="float: right"><a href="<?= $paramList ;?>">&nbsp;Reload&nbsp;</a></div>
    <form name="both_choices" method="post"><div>
        <table><tbody><tr><td>
    <input type="text" name="search" id="search" value="<?= $search_string ;?>" />
    <button type="submit"
    title="Fritekst: Søk i alle kolonnene.
    Formatet <?= $tbl_heading[2] ;?>=verdi eller <?= $tbl_heading[3] ;?>=verdi: Søk i en bestemt kolonne.
    Eksakt verdi i kolonne: <?= $tbl_heading[3] ;?>='eksempel'.
    Større eller lik verdi i kolonne: Oppdatert>2014-01-01.
    Mindre verdi i kolonne: Oppdatert<2014-01-01 ">Søk</button>
    Hvilke:
<?php
   option_list ('which',$groups,$which_opt, '', 'both_choices');
echo '

    &nbsp; Til fil
    <button type="submit" name="file_download" value="1">xls-fil</button>
 &nbsp; &nbsp; <b>'.$out_status.'</b>
    </td></tr>
    <tr><td><b>Filter</b></td></tr>
<tr  id="filter_ticks"><td>';
tabelize_arr($Filter);
// echo '<pre>'; print_r($Filter); echo '</pre>';
echo '
</td></tr></tbody></table>
</div></form><br>
';
if ($_POST['file_download'] && !$my_err) {
    ob_end_clean();
    header('Content-type: application/xls');
    header('Content-Disposition: attachment; filename="'.$which_opt.'.xls"');
    print make_xls_arr($Vis);
    exit(0);
}
    echo '<div id="one_item"></div><div class="selectable">';
    tabelize_arr($Vis);
    if ($err_msg) echo $err_msg;
    echo '</div>';
return true;
}
include 'i.php';
?>
