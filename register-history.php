<?php
$attrib_AC=4;

include '_utils/tabelize_arr.php';
include 'mysql_db.php';
include 'constants.php';
include '_medlemsregister/medlems_support.php';

function echo_text($can_store)
{
if (!isset($_POST['TABLE'])) {
    echo "Ingenting å gjøre (hist)";
    return false;
}
?>
<script type="text/javascript">
<!--
var db_table = '<?= $_POST['TABLE'] ;?>';
var endre = '<?= $_POST['endre'] ;?>';
var id = '<?= $_POST['id'] ;?>';
var can_store = <?= $can_store ;?>;

$(document).ready(function()
{
    if (endre == true) {
        $('#show_status_history').html('');
    }
    $(".history a").click(function(e){
        if (!can_store) {
            alert("Ikke tilstrekkelige rettigheter til å endre");
        } else {
            // stop normal click
            e.preventDefault();
            mrow = $(this).closest('tr');
            tindex = mrow.children('td').length - 3;
            date = mrow.children('td:eq('+tindex+')').text();
            ser_date = date.replace(/ /g,'+').replace(/:/g, '%3A');
            var form_query_string = 'Oppdatert='+ser_date+'&id='+id+'&TABLE='+db_table;
            if (confirm ('Slette?')){
                $.post("register-slett-hist.php", form_query_string,
                function(data){
                    $('#show_status_history').html(data.text);
                    if (data.status == 'OK') {
                        if (data.id) {
                            register_history(data.id, 0);
                        }
                    }
                }, "json");
            }
        }
    });
}
);
function checkHistorySubmit() {

    if (!can_store) {
        alert("Ikke tilstrekkelige rettigheter til å endre");
    } else if (confirm ('Lagre?')){
        var form_query_string = $("#change_history").serialize();
        form_query_string += '&new=true&TABLE='+db_table;
        $.post("register-gjor-endring.php", form_query_string,
        function(data){
            $('#show_status_history').html(data.text);
            if (data.status == 'OK') {
                if (data.id) {
                    register_history(data.id, 0);
                }
            }
        }, "json");
    }
}
//-->
</script>
<?php

//print '<pre>'; print_r ($post); print '</pre>';

$my_err = 0;
$id = $_POST['id'];

$conn = open_mysql();

$q = $conn->query("DESCRIBE ".$_POST['TABLE']);

$desc = array();
while($row = $q->fetch(PDO::FETCH_ASSOC)) {
    $desc[$row['Field']] = $row['Type'];
}

$query = "select * FROM ".$_POST['TABLE']." where id='".$id."' order by Oppdatert";
$result_hist = $conn->query($query);
if (!$result_hist) {
//    echo mysql_error().'<br>';
//    close_mysql($conn);
    $my_err = 1;
}

$Vis = array();

if (!$my_err) {
    // Print out result
    $rows = $result_hist->fetchall(PDO::FETCH_ASSOC);
    $number_of_rows = count($rows);
    $Vis[] = $can_store ? array_merge (array_keys($desc), array('Slett')) : array_keys($desc);
    $this_row = $desc;
    if ($number_of_rows) {
        foreach ($rows as $row) {
            $last_row = $row;
            if (isset($row['Medlem'])) {
                $row['Medlem'] = medlem_substitute (medlem_substitute_arr($conn), $row['Medlem']);
            }
            $Vis[] = $can_store ? array_merge (array_values($row), array('<a href="">x</a>')) : array_values($row);
        }
        $this_row = $row;
    }
    {
        if ($_POST['endre']) {
            foreach ($this_row as $key => $item) {
                if (!$no_of_rows) $item = '';
                $mod_item = '';
                if ($key == 'id') {
                    $mod_item = $id;
                }
                if (preg_match ('/^enum/', $desc[$key] )) {
                    $mod_item = enum_option_list($key, $item, $desc[$key]);
                } else if ($key == 'Medlem') {
                    $mod_item = medlem_select_option_list($key, $item, $conn);
                } else if ($key == 'Kommentar') {
                    $item = '';
                } else if ($key == 'Oppdatert') {
                    $item = date('Y-m-d H:i:s');
                } else if ($key == 'Av') {
                    $item = $_SESSION['USERNAME'];
                }
                if (!$mod_item) $mod_item = '<input name="'.$key.'" type="text" value="'.$item.'">';
                $mod_item .= '<input name="orig_'.$key.'" id="orig_'.$key.'" type="hidden" value="'.$item.'">';
                $this_row[$key] = $mod_item;
            }
            $Vis[] = array_values($this_row);
        }    
    }
    close_mysql($conn);
}
?>
<form id="change_history"><div class="history">
<input type="hidden" name="Endre" value="1">
<input type="hidden" name="id" value="<?= $id ;?>">

 <h4>Historikk</h4>
 <?php
    tabelize_arr($Vis);

?>
</div></form>
<?php if ($_POST['endre']) { ?>
<a href="javascript:register_history(<?= $id ;?>, 0);">&nbsp;Angre&nbsp;</a>
&nbsp;&nbsp;<a href="javascript: checkHistorySubmit();">&nbsp;Lagre&nbsp;</a>
<?php } else if ($can_store) { ?>
<br><a href="javascript:register_history(<?= $id ;?>, 1);">Tilføy</a>
<?php if (count ($Vis) < 2 ) { ?>
<a href="javascript:register_history_del_id(<?= $id ;?>, 1);">Slette hele id <?= $id ;?></a>
<?php } } 



return true;
}
header('Content-Type: text/html');
session_start(); if ($_SESSION['AC'] >= $attrib_AC) echo_text($_SESSION['AC'] > $attrib_AC ? 1 : 0); else echo 'Ingen tilgang';
?>
