<?php
$attrib_AC=5;

include '_utils/tabelize_arr.php';
include '_utils/transpose_arr.php';
include 'mysql_db.php';
include 'constants.php';
include '_medlemsregister/medlems_support.php';

function echo_text()
{
if (!isset($_POST['TABLE'])) {
    echo "Ingen databasetabell angitt (endre-medlem)";
    return false;
}
?>
<script type="text/javascript">
<!--
var db_table = '<?= $_POST['TABLE'] ;?>';
$(document).ready(function() 
{
    $('#show_status').html('');
}
);
function checkNewSubmit() {

    if (confirm ('Lagre?')){
        var form_query_string = $("#change_list").serialize();
        form_query_string += '&new=true&TABLE='+db_table;
        $.post("register-gjor-endring.php", form_query_string,
        function(data){
            // $('#show_status').html(data.text+data.status+data.id);
            if (data.status == 'OK') {
                if (data.id) {
                    form_query_string = $("#change_list_history").serialize();
                    form_query_string += '&new=true&id='+data.id+'&TABLE='+db_table+'_history';
                    $.post("register-gjor-endring.php", form_query_string,
                    function(data){
                        $('#show_status').html(data.text);
                        if (data.status == 'OK') {
                            if (data.id) {
                                register_both(0,data.id);
                            }
                        }
                    }, "json");
                }
            }
        }, "json");
    }
}
//-->
</script>
<?php

//print '<pre>'; print_r ($post); print '</pre>';

$my_err = 0;
$id = $_POST['id'];

$conn = open_mysql();
foreach (array('', '_history') as $ext) {

$q = $conn->query("DESCRIBE ".$_POST['TABLE'].$ext);

$desc = array();
while($row = $q->fetch(PDO::FETCH_ASSOC)) {
    $desc[$row['Field']] = $row['Type'];
}

$Vis = array();

if (!$my_err) {
    // Print out result
    $number_of_rows = 1;
    if ($number_of_rows) {
        if ($row = $desc) {
            $Vis[] = array_keys($row);
            $this_row = $row;
            foreach ($row as $key => $item) {
                $mod_item = '<input name="'.$key.'" type="text" value="'.$item.'">';
                if ($key == 'id') {
                    $mod_item = '<input name="'.$key.'" type="hidden" value="NULL">auto';
                } else if (preg_match ('/^enum/', $desc[$key] )) {
                    $mod_item = enum_option_list($key, $item, $desc[$key]);
                } else if ($key == 'Medlem') {
                    $mod_item = medlem_select_option_list($key, -1, $conn);
                } else if ($key == 'Oppdatert') {
                    $item = date('Y-m-d H:i:s');
                    $mod_item = '<input name="'.$key.'" type="text" value="'.$item.'">';
                } else if ($key == 'Av') {
                    $item = $_SESSION['USERNAME'];
                    $mod_item = '<input name="'.$key.'" type="text" value="'.$item.'">';
                } else {
                    $mod_item = '<input name="'.$key.'" type="text" value="">';
                }
                $mod_item .= '<input name="orig_'.$key.'" id="orig_'.$key.'" type="hidden" value="NULL">';
                $this_row[$key] = $mod_item;
            }
        }
        $Vis[] = array_values($this_row);
    } else {
        $Vis = array(array('Noe gikk galt ved generering av nytt instrument -> '.$ext));
    }
}
if ($ext == '_history') {
    echo '<h4>Historikk</h4>';
}
?>
<form id="change_list<?= $ext ;?>"><div>
<input type="hidden" name="Endre" value="1">
<?php
if ($_POST['TRANSPOSE'] == 'yes') $Vis = transpose_arr($Vis);
tabelize_arr($Vis);
?>
</div></form>
<?php
}
close_mysql($conn);
{ ?>
&nbsp;&nbsp;<a href="javascript: register_both(0,0);">&nbsp;Få meg ut herfra&nbsp;</a>
&nbsp;&nbsp;<a href="javascript: checkNewSubmit();">&nbsp;Lagre&nbsp;</a>
<?php } 



return true;
}
header('Content-Type: text/html');
session_start(); if ($_SESSION['AC'] >= $attrib_AC) echo_text(); else echo 'Ingen tilgang';
?>
