<?php
$attrib_AC=4;
$attrib_W=320;

function find_type ()
{
return $_GET['reg'];
}


function echo_javascript()
{
$type = find_type();
?>
<script type="text/javascript">

var id = 0;

<!--//--><![CDATA[//><!--
var row = 0;
var ticked_only = 0;
function register_list(new_id, endre) {
	id = new_id;
	$('#display_list').load("register-list.php", {id: id, TABLE: '<?= $type ;?>', TRANSPOSE: '<?= $_GET['TRANSPOSE'] ;?>', endre: endre });
}
function register_history(new_id, endre) {
	id = new_id;
	$('#display_history').load("register-history.php", {id: id, TABLE: '<?= $type ;?>_history', endre: endre });
}
function register_history_del_id(id, endre) {
	if (confirm("Slette hele id "+id+"?")) {
                $.post("register-slett-id.php", 'id='+id+'&TABLE=<?= $type ;?>',
                function(data){
                    $('#display_list').html(data.text);
                }, "json");
	}
}
function register_new_id() {
	$('#inst_nr').html('auto');
	$('#display_history').html('');
	$('#display_list').load("register-new-id.php", {id: 'new', TABLE: '<?= $type ;?>', TRANSPOSE: '<?= $_GET['TRANSPOSE'] ;?>'});
}
function register_both(inc, id) {
	$('#display_history').html('vent...');
	$('#display_list').html('vent...');
	var length = $('.selectable tr').length;
	do {
	row += inc;
	if (row >= length) {
		row = 1;
	} else if (row <= 0) {
		row = length-1;
	}
	var ticked_this = $('.selectable tr:eq('+row+') td:eq(0) input').is(':checked');
	var inst_nr_extra = 1;
	if (id) {
		row = 0;
		inst_nr_extra = 0;
	} else {
		if (!inc) {
			ticked_only = ticked_this;
		} else if (ticked_only && !ticked_this) {
			continue;
		}
		id = $('.selectable tr:eq('+row+') td:eq(1)').html();
	}
	$('#inst_nr').html((ticked_this ? '<input type="checkbox" checked disabled/>' : '<input type="checkbox" disabled/>')+' id '+id+(inst_nr_extra ? ' - linje '+row : ''));
	break;
	} while (1);
	register_list(id, 0);
	register_history(id, 0);
}
function clear_page() {
	$('#one_item').html('');
	$('.selectable').show();
}
$(document).ready(function() 
{ 
//    $('.view').show();
//    $('.modify').hide();
      row = parseInt(<?= $_GET['row'] ;?>+0);
	register_both(0,0);
}
);
//--><!]]>
</script>
<?php	
}

function echo_text($al)
{

echo_javascript();

$row = 0;
if (isset ($_GET['row'])) {
    $row = $_GET['row'];
}
$type = find_type();
$id = "Ikke definert";
echo '<h3><span id="inst_nr">'.$id.'</span></h3>';
if ($type == '') return;
?>

<div>
<a href="javascript: register_both(-1,0);">&nbsp;&lt;&nbsp;</a>  &nbsp;
<a href="javascript: register_both(1,0);">&nbsp;&gt;&nbsp;</a> &nbsp;
<?php if ($al > 4) { ?>
<a href="javascript: register_new_id();">&nbsp;nytt&nbsp;</a> &nbsp; &nbsp; &nbsp;
<?php } ?>
<a href="javascript: clear_page();">&nbsp;tilbake&nbsp;</a>
</div>
<?php if ($_GET['TRANSPOSE'] == 'yes') { ?>
<div id="force_height" style="float: left; height: 600px; padding: 1px; background: rgba(230,230,250, 0.9);"></div>
<div style="float: left; padding-right: 20px;">
<?php } else { ?>
<div>
<?php } ?>

<span id="display_list"></span>
 <span id="show_status"></span>
</div>
<div>
<span id="display_history"></span>
 <span id="show_status_history"></span>
</div>
<?php

return true;
}
header('Content-Type: text/html');
session_start(); if ($_SESSION['AC'] >= $attrib_AC) echo_text($_SESSION['AC']); else echo 'Ingen tilgang';
?>
